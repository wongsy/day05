/**
 * Created by s25854 on 14/10/2016.
 */
//Exercise 5.2: Access and Display Request / Response Messages
var express = require('express');
var app = express();

const PORT = process.argv[2] || 3000;
const CLIENT_FOLDER = __dirname + "/../client";
app.use(express.static(CLIENT_FOLDER));
app.use("/bower_components", express.static(__dirname + "/../bower_components")); //this line can be removed wiht bowerrc

/*
4. Handle route /showReq:
handler should display 5 request object information, for example, req.method
as a response, handler should send a json object of 5 response header fields.
*/
app.use('/showReq', function(req, res) {
    // console.log("showreq started...");
    // console.log("baseUrl: ", req.baseUrl);
    // console.log("hostname: ", req.hostname);
    // console.log("Path: ", req.path);
    // console.log("Cookies: ", req.cookies);
    // console.log("Query: ", req.query);
    //
    // //Sends a JSON response
    // res.json({
    //     'app': res.app,
    //     'headersent': req.headersent,
    //     'locals': req.locals
    // });

    var reqProperties = {
        hostName: req.hostName,
        baseUrl: req.baseUrl,
        cookies: req.cookies,
        ip: req.ip,
        secure: req.secure
    };
    // Solution 4a
    console.log(reqProperties);

    // Solution 4b - you can also use .send and pass a string
    res.json(reqProperties);
});

//Create a route handler for POST, PUT, and DELETE
//You may test these routes using https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=en
app.get("/sample", function (req, res) {
    res.json({
        ok: true
    })
});
app.put("/sample", function (req, res) {
    res.json({
        ok: true
    })
});
app.post("/sample", function (req, res) {
    res.json({
        ok: true
    })
});
//5a - 404 Handler - Page not found
app.use(function(req, res) {
    res.redirect('/404.html');
    //res.status(404).redirect('/404.html');
});

//5b - 501 Handler
//to simulate error 501 being called
// To test, 404 Handler must be commented out
// app.use('/page-that-throw-error', function(req,res){
//     console.log("error 501");
//     throw new Error("Simple Custom Error");
// })

app.use(function(err, req, res, next) {
   // res.status(501).redirect('/501.html');
    res.redirect('/501.html');
});

// Starts the server on localhost (default)
app.listen(PORT, function(){
    console.log("Server listening on: http://localhost:%s", PORT);
});

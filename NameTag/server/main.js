/**
 * Created by s25854 on 14/10/2016.
 */

var express = require('express');
var app = express();

const PORT = process.argv[2] || 3000;
const CLIENT_FOLDER = __dirname + "/../client";
app.use(express.static(CLIENT_FOLDER));

//404 Handler - Page not found
app.use(function(req, res) {
   res.redirect('/404.html');
   // res.sendFile(__dirname + '/../client/404.png');
});

//501 Handler
app.use(function(err, req, res, next) {
    res.redirect('/501.html');
});

// Starts the server on localhost (default)
app.listen(PORT, function(){
    console.log("Server listening on: http://localhost:%s", PORT);
});
